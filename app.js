const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const res = require("express/lib/response");
const { use } = require("express/lib/application");
const { add } = require("nodemon/lib/rules");
const moment = require("moment");
const { json } = require("express/lib/response");
let PORT = 6000;
const URL =
  "mongodb+srv://harish_aerate:harish1234@cluster0.ygbab.mongodb.net/tableTennis?retryWrites=true&w=majority";
const app = express();
app.use(express.json());
app.use(bodyParser.json());

//MongoDB Connection
mongoose
  .connect(URL)
  .then((res) => console.log("Successfull"))
  .catch((err) => console.log("Error"));

const UserSchema = new mongoose.Schema({
  first_name: { type: String },
  last_name: { type: String },
  email: { type: String, unique: true, required: true, immutable: true},
  admin: { type: Boolean },
  profile_picture: { 
    data: Buffer,
    contentType: String },
});

const SlotSchema = new mongoose.Schema({
  date: {
    type: String,
    required: true,
  },
  slot: {
    type: String,
    required: true,
  },
  bookedAt: {
    type: String,
    default: moment(),
  },
  email: {
    type: String,
  },
});

app.get("/", (req,res) => {
  console.log("base endpoint...");
  res.send( {status: "ok"} )
})


//API to Add User Data
app.post("/api/adduser", (req, res) => {
  let first_name = req.body.first_name;
  let last_name = req.body.last_name;
  let inputEmail = req.body.email;
  let email = inputEmail.trim();
  if (first_name == "" || last_name == "" || email == "") {
    console.log("User cannot be added, fields are missing");
    res.send({status: 0, message: "Fields missing, please check"});
  } else {
    addUser(first_name, last_name, email, res);
  }
});

//Function to add user
function addUser(first_name, last_name, email, res) {
  const UserInfo = mongoose.model("users", UserSchema);
  if(email.includes('@avaamo.com')){
  UserInfo.findOne({ email: `${email}` })
  .then((result) => {
    if (result) {
      res.send({status: 0, email: `${email}`, message: 'User already exists'});
    } 
    else 
    {
      let users = new UserInfo({
        first_name: first_name,
        last_name: last_name,
        email: email,
        admin: false,
        profile_picture: null,
      });
      users
        .save()
        .then((result) => {
          res.send({ status: 1, email: `${email}`, message: 'User registered successfully' });
        })
        .catch((err) => {
          console.log(err);
          res.send({ status: 0, message: 'Something went wrong!!!' });
        });
    }
  });
}
else{
  res.send({status: 0, email: `${email}`, message: 'Not an avaamo member!!!'});
}
}

//API to Delete User Data
app.post("/api/deleteuser", (req, res) => {
  let inputEmail = req.body.email;
  let email = inputEmail.trim();
  if (email == "" || email == null) {
    res.send("Field missing, please check");
  } else {
    deleteUser(email, res);
  }
});

//Function to delete user
function deleteUser(email, res) {
  console.log(email);
  const UserInfo = mongoose.model("users", UserSchema);
  const SlotInfo = mongoose.model("slots", SlotSchema);
  UserInfo.findOneAndRemove({ email: `${email}` })
    .then((result) => {
      if(result){
        SlotInfo.deleteMany({email: email})
        .then((res) => {
          console.log(res);
        })
        res.send({ status: 1, email: `${email}`, message: 'Successfully removed the user' });
      }
      else{
        res.send({status: 0, email: `${email}`, message: "User doesn't exist"});
      }
    })
    .catch((err) => {
      console.log(err);
      res.send({ status: 0, message: 'Something went wrong!!!' });
    });   
}

//API to edit a specific user
app.post("/api/updateuser", (req, res) => {
  let first_name = req.body.first_name;
  let last_name = req.body.last_name;
  let inputEmail = req.body.email;
  let email = inputEmail.trim();
  let admin = req.body.admin;
  let profile_picture = req.body.profile_picture;
  if (first_name == "" || last_name == "" || email == "") {
    res.send({status: 0, message: 'Fields missing, please check!'});
  } else {
    updateUser(first_name, last_name, email, admin, profile_picture, res);
  }
});

//Function to update/edit a specific user
function updateUser(first_name, last_name, email, admin, profile_picture, res) {
  const UserInfo = mongoose.model("users", UserSchema);
  if(email.includes('@avaamo.com')){
  UserInfo.find({email: email})
  .then((result) => 
  {
    console.log(result);
    if(result.length == 0){
      res.send({status: 0, message: 'You cannot update email id. Update only first name, last name, profile_picture'});
    }
    else
    {
      UserInfo.updateMany(
        { email: `${email}` },
        {
          $set: {
            first_name: `${first_name}`,
            last_name: `${last_name}`,
            admin: `${admin}`,
            profile_picture: `${profile_picture}`,
          },
        }
      )
        .then((result) => {
          res.send({ status: 1, email: `${email}`, message: 'User data updated successfully' });
        })
        .catch((error) => {
          console.log(error);
          res.send({ status: 0, message: 'Something went wrong!!!' });
        }); 
    }
  })
  .catch((error) => {
    console.log(error, "Something went wrong!")
  })
  }
  else{
    res.send({status: 0, email: `${email}`, message: 'Not an avaamo member!!!'})
  }
}

//API to know whether a specific user is registered or not
app.post("/api/getuser", (req, res) => {
  let inputEmail = req.body.email;
  let email = inputEmail.trim();
  if (email == "" || email == null) {
    res.send({status: 0, message: 'Fields missing, please check!'});
  } else {
    getUser(email, res);
  }
});

//Function to get registered user if present
function getUser(email, res) {
  const UserInfo = mongoose.model("users", UserSchema);
  UserInfo.find({ email: `${email}` })
    .then((resp) => {
      if (resp.length > 0) {
        res.send({ status: 1, email: `${email}`, message: 'User is registered', output: {
          first_name: resp[0]. first_name,
          last_name: resp[0].last_name,
          email: resp[0].email,
          profile_picture: resp[0].profile_picture
        }
       });
      } 
      else 
      {
        res.send({ status: 0, email: `${email}`, message: 'User is not registered' });
      }
    })
    .catch((err) => {
      console.log(err)
      res.send({status: 0, message: 'Something went wrong!!!', error: err})
    });
}

//API to book a slot using email_id
app.post("/api/bookslot", (req, res) => {
  let inputEmail = req.body.email;
  let email = inputEmail.trim();
  let date = req.body.date;
  let dateInputRev = date.split("-").reverse().join("-");
  let fullDate = new Date(dateInputRev);
  let day = fullDate.getDay();
  let dateInput = Date.parse(dateInputRev);
  let today = new Date().toISOString().slice(0, 10);
  let todaysDate = Date.parse(today);
  let slot = req.body.slot;
  let bookedAt = moment();

  if(email == "" || date == "" || slot == "") {
    res.send({status: 0, message: "Fields missing, please check"});
  }
  else if(dateInput < todaysDate){
    res.send({status: 0, message: 'You should not enter past dates'})
  }
  else if(day == 0 || day == 6){
    res.send({status: 0, message: 'Its a Holiday!!'})
  }
  else if(date.slice(0,2) > 31 || date.slice(3, 5) > 12){
    // console.log(dateValue, month)
    res.send({status: 0, message: 'Enter valid date and month'})
  }
   else {
    bookSlot(email, date, slot, bookedAt, res);
  }
});

//Function to book slot (Add userinfo to the slot data)
function bookSlot(email, date, slot, bookedAt, res) {
  const UserInfo = mongoose.model("users", UserSchema);
  const SlotInfo = mongoose.model("slots", SlotSchema);
  if(email.includes('@avaamo.com'))
  {
  UserInfo.find({ email: `${email}` })
    .then((result) => {
      if (result.length === 1) {
        SlotInfo.find({ date: `${date}`, slot: `${slot}` })
        .then((resp) => {
          if (resp && resp.length > 0) {
            console.log("Booking already exists");
            res.send({ status: 0, message: "Booking already exists" });
          } else {
            let slots = new SlotInfo({
              date: date,
              slot: slot,
              bookedAt: bookedAt,
              email: email,
            });

            //Saves booked data
            slots
              .save()
              .then((result) => {
                res.send({
                  status: 1,
                  message: "Saved Successfully",
                  data: result,
                });
              })
              .catch((err) => {
                console.log(err);
                res.send({ status: 0, message: err });
              });
          }
        });
      } else {
        res.send({status: 0, message: "User not found, please add the user first"});
        console.log("User not found, please add the user first");
      }
    })
    .catch((error) => {
      console.log("Something went wrong!!!", error);
    });
  }
  else
  {
    console.log('Not an avaamo member!!!')
    res.send({status: 0, email: `${email}` ,message: 'Not an avaamo member!!!'});
  }
}

//API to Reschedule Slot
app.post("/api/reschedule", (req, res) => {
  let date = req.body.date;
  let new_date = req.body.newDate;
  let slot = req.body.slot;
  let new_slot = req.body.newSlot;
  let inputEmail = req.body.email;
  let email = inputEmail.trim();
  if (date == "" || slot == "" || email == "") {
    res.send({status: 0, message: "Fields missing, please check"});
  } else {
    rescheduleSlot(date, slot, new_date, email, new_slot, res);
  }
});

//Function to reschedule slot booked
function rescheduleSlot(date, slot, new_date, email, new_slot, response) {
  const SlotInfo = mongoose.model("slots", SlotSchema);
  if(email.includes('@avaamo.com'))
  SlotInfo.find({ email: `${email}`, date: `${date}`, slot: `${slot}` })
  .then((res) => {
    if (res.length > 0) {
      SlotInfo.find({slot: `${new_slot}`, date: `${new_date}`})
      .then((res) => {
        if(res.length > 0){
          console.log('Booking already exists with the new date and slot');
          response.send({status: 0, message: `Booking already exist on ${new_date}, ${new_slot}`})
        }
        else
        {
          SlotInfo.updateOne(
            { email: `${email}`, date: `${date}`, slot: `${slot}` },
            {
              $set: {
                date: `${new_date}`,
                slot: `${new_slot}`,
              },
            }
          )
          .then((result) => {
            console.log("Successfull");
            response.send({ status: 1, email: `${email}`, message: "Rescheduled Successfully" });
          });
        }
      })
      
    } else {
      response.send({
        status: 0,
        email: `${email}`,
        message: "No matching data found for this date and user",
      });
    }
  })
  .catch((error) => {
    res.send({status: 0, message: 'Something went wrong!!!'})
  })
}

//API to Cancel Slot
app.post("/api/cancelslot", (req, res) => {
  let date = req.body.date;
  let slot = req.body.slot;
  let inputEmail = req.body.email;
  let email = inputEmail.trim();
  if (date == "" || slot == "" || email == "") {
    res.send("Fields missing, please check");
  } else {
    cancelSlot(date, slot, email, res);
  }
});

//Function to cancel slot - User have to give date, slot and email-id
function cancelSlot(date, slot, email, response) {
  const SlotInfo = mongoose.model("slots", SlotSchema);
  SlotInfo.findOneAndRemove({
    date: `${date}`,
    slot: `${slot}`,
    email: `${email}`,
  }).then((result) => {
    if(result){
    response.send({ status: 1, email: `${email}`, message: "Booking cancelled successfully" });
    }
    else{
      response.send({ status: 0, email: `${email}`, message: "No bookings found" });
    }
  })
  .catch((error) => {
    response.send({status: 0, message: 'Something went wrong!!!'})
  })
}

//Get free slots based on the date user enters
app.post("/api/freeslots", (req, res) => {
  let date = req.body.date;
  console.log("IN FREE SLOTS=>>>>>>> ", req)
  getFreeSlots(date, res);
});

//Function to get free slots
function getFreeSlots(date, res) {
  const SlotInfo = mongoose.model("slots", SlotSchema);
  let slotsObj = [
    { time: "9:00AM-10:00AM", isBooked: false },
    { time: "10:00AM-11:00AM", isBooked: false },
    { time: "11:00AM-12:00PM", isBooked: false },
    { time: "12:00PM-1:00PM", isBooked: false },
    { time: "1:00PM-2:00PM", isBooked: false },
    { time: "2:00PM-3:00PM", isBooked: false },
    { time: "3:00PM-4:00PM", isBooked: false },
    { time: "4:00PM-5:00PM", isBooked: false },
    { time: "5:00PM-6:00PM", isBooked: false },
    { time: "6:00PM-7:00PM", isBooked: false },
  ];
  SlotInfo.find({ date: `${date}` })
    .then((result) => {
      if (result.length > 0) {
        result.forEach((ele) => {
          slotsObj.filter((item) => {
            if (ele.slot == item.time) {
              return (item.isBooked = true);
            }
          });
        });
        res.send({status: 1, output: slotsObj});
      } else {
        res.send({ status: 1, message: "All slots are free", output: slotsObj });
      }
    })
    .catch((error) => {
      res.send({ status: 0, message: error });
    });
}

//Get booked slots based on email user enters
app.post("/api/getbookedslots", (req, res) => {
  let email = req.body.email;
  if (email == "") {
    res.send({status: 0, message: 'Fields missing, please check!'});
  } else {
    getBookedSlots(email, res);
  }
});

//Function to get all the booked slots of specific user
function getBookedSlots(email, res) {
  // console.log(email);
  const SlotInfo = mongoose.model("slots", SlotSchema);
    SlotInfo.find({email: email })
    .then((result) => {
        if (result.length > 0) {
          res.send({status: 1, email: `${email}`, output: result});
        } else {
          res.send({ status: 0, email: `${email}`, message: "No bookings found" });
        }
    })
    .catch((error) => {
      res.send({ status: 0, message: "Somethong went wrong!!!" });
    });
}

//Server listening
PORT = process.env.PORT || PORT
app.listen(PORT, () => {
  console.log(`Server listening to port ${PORT} at ${moment()}`);
});